$("edit-answer[id^=edit_]").on("click", function () {
    // Hide the edit button
    $(this).slideToggle("fast");

    var answerID = event.target.id.slice(5);
    // Get the current body of the question so the edit will start with this text prepended.
    var questionBody = $("#"+answerID).children("pre").text();
    // Create our form which contains the previous body
    formHtml = `
        <form role="form" id="form_${answerID}">
            <div class="form-group">
                <textarea type="text" rows="10" name="body" class="form-control">${questionBody}</textarea>
            </div>
            <button class="btn btn-default" type='submit'>Submit</button>
            <button class="btn btn-primary" type='button' id="cancel_${answerID}">Cancel</button>
        </form>
    `
    // Replace the pre with our new form
    $("#"+answerID).empty().append(formHtml);
});

$(document).on("click", "[id^=cancel_]",function () {
    // Get the ID of the answer
    var answerID = event.target.id.slice(7);
    // Get the original content of the textarea
    var questionBody = $("#"+answerID).find("textarea").text();
    preHtml = `
        <pre>${questionBody}</pre>
    `
    // Replace the form with the original content
    $("#"+answerID).empty().append(preHtml);
    // Bring back the edit button
    $("#edit_"+answerID).slideToggle("fast");
});

$(document).on("submit", "[id^=form_]",function (event) {
    // Prevent form submit from reloading the page
    event.preventDefault();
    // Get the ID of the answer
    var answerID = event.target.id.slice(5);
    // Get the form data
    var body = $(this).serializeArray()[0];

    $.ajax({
        url: '/answers/answer/'+answerID,
        type: 'PUT',
        data: {'body': body.value},
        success: function(data,status){
            var preHtml = `
                <pre>${data.body}</pre>
            `
            // Replace the form with the original content
            $("#"+answerID).empty().append(preHtml);
            // Bring back the edit button
            $("#edit_"+answerID).slideToggle("fast");
            // Set the last edit date under the answer body
            var edited = `
                <div class="text-right">edited on ${moment(data.edited).format("DD.MM.YYYY")} at ${moment(data.edited).format("HH:mm")}</div>
            `
            $("#edited_"+answerID).empty().append(edited);
        }
    });
});