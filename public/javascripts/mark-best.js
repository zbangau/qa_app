$("edit-answer[id^=best_]").on("click", function () {
    // Hide the mark button
    $(this).slideToggle("fast");
    var answerID = event.target.id.slice(5);
    var questionElement = $("#qid");
    var questionId = questionElement.val();
    $.ajax({
        url: '/questions/select/'+ questionId,
        type: 'POST',
        data: {'answerId': answerID},
        success: function(data,status){
            location.reload();
        }
    });
});