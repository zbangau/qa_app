$("div[name=btnPlus]").on("click", function () {
    var divId = event.target.id;
    var answerID = divId.slice(2);
    $.post('/questions/points',
        {
            answerId: answerID,
            points: 1
        },
        function(data,status){
            domId = "#points_"+answerID;
            $(domId).empty().append(data.points);
        });
});

$("div[name=btnMinus]").on("click", function () {
    var divId = event.target.id;
    var answerID = divId.slice(2);
    $.post('/questions/points',
        {
            answerId: answerID,
            points: -1
        },
        function(data,status){
            domId = "#points_"+answerID;
            $(domId).empty().append(data.points);
        });
});