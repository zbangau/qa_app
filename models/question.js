var mongoose = require('mongoose');
var Schema = mongoose.Schema;

const getTags = tags => tags.join(',');
const setTags = function (tags) {
    tagsList = tags.split(',');
    for (i in tagsList) {
        tagsList[i] = tagsList[i].trim();
    }
    console.log(tagsList);
    return tagsList;
}

var QuestionSchema = new Schema({
    title: { type: String, default: '', trim: true },
    body: { type: String, default: '', trim: true },
    user: { type: Schema.ObjectId, ref: 'Account' },
    solved: { type: Boolean, default: false },
    views: { type: Number, default: 0 },
    answers: [{
        answer: { type: Schema.ObjectId, ref: 'Answer' },
        isBest: { type: Boolean, default: false }
    }],
    tags: { type: [], get: getTags, set: setTags },
    dateAsked: { type: Date, default: Date.now }
});

QuestionSchema.methods = {
    addAnswer: function(user, answer){
        this.answers.push(answer._id);
    },

    deleteAnswer: function(user, answer){
        this.answers.pop(answer._id);
    },

    selectBestAnswer: function(user, answer){
        this.solved = true;
        for (index = 0; index < answers.length; ++ index){
            if (answers[index].answer == answer._id){
                answers[index].isBest = true;
                return;
            }
        }
    }
}

QuestionSchema.index({title: "text", body: "text"});

module.exports = mongoose.model('Question', QuestionSchema);