var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnswerSchema = new Schema({
    body: { type: String, default: '', trim: true },
    points: { type: Number, default: 0},
    datePosted: { type: Date, default: Date.now },
    lastModified: {type: Date, default: null},
    user: { type: Schema.ObjectId, ref: 'Account' },
    _userPoints: [{
        value: { type: Number, default: 1 },
        user: { type: String, default: '' }
    }]
});

// Body is required
AnswerSchema.path('body').required(true, 'Answer body cannot be blank');

AnswerSchema.methods = {
    addPoints: function (username, pointsValue) {
        pointsValue = parseInt(pointsValue);
        var found = false;
        for ( index = 0; index < this._userPoints.length; ++index ){
            if (this._userPoints[index].user == username){
                var oldRating = this._userPoints[index].value; 

                if (oldRating == pointsValue){
                    // If a comment had a + from a user and the user pressed + again, we don't need to do anything
                    return;
                }

                // Add the rating to the ratings array
                this._userPoints[index].value = pointsValue;
                // Adapt the total ratings counter
                this.points -= oldRating;
                this.points += pointsValue;
                found = true;
                break;
            }
        }
        if (found == false){
            // First time the user adds a rating to this answer, so we just add it as is.
            this._userPoints.push({
                value: pointsValue,
                user: username
            });
            this.points += pointsValue;
        }
        console.log(this._userPoints);
    }
}

AnswerSchema.statics = {
    /*
    Find answer by id
    */
    load: function (_id) {
        return this.findOne({ _id })
          .populate('user', 'email username')
          .populate('_userPoints.user')
          .exec();
    },
    list: function (options) {
        const criteria = options.criteria || {};
        const page = options.page || 0;
        const limit = options.limit || 30;
        return this.find(criteria)
          .populate('user', 'name username')
          .sort({ datePosted: -1 })
          .limit(limit)
          .skip(limit * page)
          .exec();
        }
}

module.exports = mongoose.model('Answer', AnswerSchema);