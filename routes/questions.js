var express = require('express');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var router = express.Router();
var cachegoose = require('cachegoose');

var Question = require('../models/question');
var Answer = require('../models/answer');

router.use(function(req, res, next){
    if (req.user != null){
        next();
    } else {
        console.log("Non Authorized.");
        res.redirect('/');
    }
});

// Patch mongoose for redis caching
cachegoose(mongoose, {
  engine: 'redis',
  port: 6379,
  host: 'localhost'
});

// New question form
router.get('/ask', function (req, res) {
    res.render('question/ask', { user : req.user , topQuestions: req.topQuestions });
});

// New q60*5uestion is created
router.post('/ask', function (req, res) {
    var question = new Question({
        title: req.body.title,
        body: req.body.body,
        tags: req.body.tags,
        user: req.user._id
    });
    question.save(function(err){if (err) console.log(err)});
    res.redirect('/');
});

// Shows all the questions asked by the registered user
router.get('/list', function(req, res) {
    userId = req.user._id;
    Question.find({"user": userId})
        .select("_id title body tags")
        .exec(
            function(err, obj){
                if (err){
                    console.log(err);
                } else {
                    res.render('question/list', { questions: obj, user: req.user, topQuestions: req.topQuestions, source: "navbar" });
                }
        });
});

// POST for adding points to answers
router.post('/points', function(req, res){
    // Find the answer which is given points
    var id = ObjectId(req.body.answerId);
    query = Answer.findOne(id, function(err, obj){
        if (err){
            console.log(err);
        } else {
            // Call the addPoints method
            obj.addPoints(req.user, req.body.points);
            obj.save();

            // Send a json response so the UI can update the points counter
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({"points": obj.points}));
        }
    });
});

sortAnswersValue = function(a,b) {
    if (a.isBest){
        return -1;
    }
    if (b.isBest){
        return 1;
    }
    var scoreA = a.answer.points;
    var scoreB = b.answer.points;
    if (scoreA < scoreB) return 1;
    if (scoreA > scoreB) return -1;
    return 0;
}

// Shows the details of a single question
router.get('/show/:id', function(req, res) {
    // Find our question
    query = Question.findOne({"_id" : req.params.id});
    // Select the fields we need
    query.select("title body tags answers user views");
    // Load data from foreign tables
    query.populate("user", "username");
    query.populate({path: "answers.answer",
                    model: "Answer",
                    populate: { path: "user",
                                model: "Account"}});
    // Execute the query
    query.exec(function(err, obj){
        if (err){
            console.log(err);
        } else {
            // Increase the views counter
            obj.views = obj.views + 1;
            obj.save();

            userAnswered = false;
            allAnswers = obj.answers;
            for (index = 0; index < allAnswers.length; ++ index){
                if (allAnswers[index].answer.user.username == req.user.username){
                    userAnswered = true;
                }
            }
            isCreator = obj.user.username == req.user.username? true: false;
            obj.answers.sort(sortAnswersValue);
            res.render('question/detail', {
                foundQuestion: obj,
                user: req.user,
                isCreator: isCreator,
                answered: userAnswered,
                topQuestions: req.topQuestions });
        }
    });
});

// Selecting a best answer for a question
router.post('/select/:id', function(req, res) {
    // Find our question
    query = Question.findOne({"_id" : req.params.id});
    // Select the fields we need
    query.select("title body tags answers user views");
    // Load data from foreign tables
    query.populate("user", "username");
    query.populate({path: "answers.answer",
                    model: "Answer",
                    populate: { path: "user",
                                model: "Account"}});
    // Execute the query
    query.exec(function(err, obj){
        if (err){
            console.log(err);
        } else {
            if(req.user.username != obj.user.username){
                res.status(403).send("Error, only the creator of the question can select the best answer!");
                return;
            }
            for (i = 0; i < obj.answers.length; ++ i){
                if (obj.answers[i].answer._id == req.body.answerId){
                    obj.answers[i].isBest = true;
                } else {
                    obj.answers[i].isBest = false;
                }
            }
            obj.save();
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({"selected": true}));
        }
    });
});

// Route for posting a new answer to a question
// NOTE: The id is the id of the question.
router.post('/answer/:id', function(req, res) {
    // Find our question
    query = Question.findOne({"_id" : req.params.id});
    // Select the fields we need
    query.select("title body tags answers user");
    // Load data from foreign tables
    query.populate("user", "username");
    query.populate({path: "answers.answer",
                    model: "Answer",
                    populate: { path: "user",
                                model: "Account"}});
    // Execute the query
    query.exec(function(err, obj){
        if (err){
            console.log(err);
        } else {

            allAnswers = obj.answers;
            for (index = 0; index < allAnswers.length; ++ index){
                if (allAnswers[index].answer.user.username == req.user.username){
                    res.setHeader('Content-Type', 'application/json');
                    res.status(400).send(JSON.stringify({"error": "you have already answered this question!"}));
                    return;
                }
            }
            // Create the new answer
            var answer = new Answer({
                body: req.body.body,
                user: req.user._id
            });
            answer.save(function(err){if (err) console.log(err)});

            // Bind it to our question
            obj.answers.push({'answer': answer, 'isBest': false});
            obj.save(function(err){if (err) console.log(err)});

            // Redirect to the question's page
            res.redirect('/questions/show/'+req.params.id);
        }
    });
})

// Search for a question
router.get('/search', function(req, res) {
    if (!req.query.searchText){
        res.render('question/list', { error: {message: "Missing parameter: searchText"}, questions: [], user: req.user, topQuestions: req.topQuestions, source: "search" });
    }
    Question.find(
        { $or: [
            { title: new RegExp(req.query.searchText, 'i') }, // search by title
            { body: new RegExp(req.query.searchText, 'i') }, // body
            { tags: new RegExp(req.query.searchText, 'i') } // tag
            ] 
        }
    )
    .select ("title body tags views user")
    .populate("user", "username")
    .exec (function(err, results){
        if (err) { console.log(err); }
        res.render('question/list', { questions: results, user: req.user, topQuestions: req.topQuestions, source: "search" });
    });
});

module.exports = router;

router.get('/tag/:id', function(req,res) {
    var tag = req.params.id;
    Question.find(
        { tags: new RegExp(tag, 'i') } // tag
    )
    .select ("title body tags views user")
    .populate("user", "username")
    .exec (function(err, results){
        if (err) { console.log(err); }
        res.render('question/list', { questions: results, user: req.user, topQuestions: req.topQuestions, source: "tag_" + tag });
    });
});
