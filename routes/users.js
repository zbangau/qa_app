var express = require('express');
var mongoose = require('mongoose');
var redis = require("redis");

var ObjectId = mongoose.Types.ObjectId;
var router = express.Router();

var redisClient = redis.createClient();
var Answer = require('../models/answer');
var Account = require('../models/account');
var Question = require('../models/question');
var moment = require('../public/javascripts/libraries/moment.min')

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with a resource');
});

var mapReduce = {}

// This function will map all the answer points to one username
mapReduce.map = function(){
    emit(this.user, this.points);
}

mapReduce.reduce = function(key, values){
    return Array.sum(values)
}

continueRequest = function(mappingOfIdsAndPoints, req, res){
    var mapping = {};
    for (entry in mappingOfIdsAndPoints){
        mapping[mappingOfIdsAndPoints[entry]._id] = mappingOfIdsAndPoints[entry].value;
    }
    Account.find()
        .where('_id')
        .in(Object.keys(mapping))
        .exec(function(err, results){
            finalResults = []
            for (index in results){
                entry = {
                    value: mapping[results[index]._id],
                    username: results[index].username
                }
                finalResults.push(entry);
            }
            res.render('users/ranking', { topUsers: finalResults.sort(sortByValue), user: req.user , topQuestions: req.topQuestions });
        });
}

sortByValue = function(a,b) {
    var scoreA = a.value;
    var scoreB = b.value;
    if (scoreA < scoreB) return 1;
    if (scoreA > scoreB) return -1;
    return 0;
}

// We will use the map-reduce method for obtaining the points of each user
router.get('/ranking', function(req, res) {
    // First, check if the cache is recent since this would mean no map-reduceing

    // We need to aggregate all the scores which the user has received
    // and select the top 30.
    var nowDate = moment();
    var redisDate = redisClient.hgetall("rankings_cache", function(err, value){
        if (err) throw (err);
        if (value)
            var redisDate = moment(value.date_created, "DD.MM.YYYY-HH:mm:ss");

        // Check if cache is still recent
        if (!value || nowDate.diff(redisDate) > 60 * 1000 * 15){
            // Update cache
            // Perform map-reduce to get data
            Answer.mapReduce(mapReduce, function(err, results){
                if (err) {
                    console.log(err);
                    res.redirect('/');
                }
                // We need to sort the results descendingly
                results.sort(sortByValue);
                // Take top 30 users
                topUsers = results.slice(0,29);
                topUsersAsString = JSON.stringify(topUsers);
                // Update redis cache
                redisClient.del("rankings_cache")
                redisClient.hmset("rankings_cache", ["date_created", moment(new Date()).format("DD.MM.YYYY-HH:mm:ss"), "top_users", topUsersAsString],
                    function(err, res){
                        if (err) {
                            console.log(err);
                        }
                    });
                continueRequest(topUsers, req, res);
            });
        } else {
            // Get the top users from cache
            var topUsers = JSON.parse(value.top_users);
            continueRequest(topUsers, req, res);
        }
    });

    
});

router.get('/:id', function(req, res) {
    username = req.params.id;
    Account.findOne({"username": username})
        .select("_id")
        .exec(function(err, obj){
            if (err){
                console.log(err);
            } else {
                var userId = obj._id;
                Question.find({"user": userId})
                .select("_id title body tags")
                .exec(
                    function(err, obj){
                        if (err){
                            console.log(err);
                        } else {
                            res.render('question/list', { questions: obj, user: req.user, topQuestions: req.topQuestions, source: username });
                        }
                });
            }
    });
});

module.exports = router;
