var express = require('express');
var passport = require('passport');
var Account = require('../models/account');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { user : req.user,
                topQuestions: req.topQuestions });
});

router.get('/register', function(req, res) {
    res.render('register', { });
});

function validateEmail(email) {
  var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

function errorsInData(data) {
    if ( data.username.length < 6 ) {
        return "Username too short, must be at least 6 characters long.";
    }
    if ( data.password.length < 8 ) {
        return "Password too short, must be at least 8 characters long.";
    }
    if ( data.password != data.passwordConfirm ) {
        return "Passwords don't match.";
    }
    if ( !validateEmail(data.email) ) {
        return "E-mail is not a valid formatted e-mail.";
    }
    return false;
}

router.post('/register', function(req, res) {
    var errors = errorsInData(req.body)
    if (errors) {
        res.render('register', {error: {message: errors}});
    } else {
        Account.register(new Account({ username : req.body.username }), req.body.password, function(err, account) {
            if (err) {
                var error = { error: {message: err.message} }
                console.log(error);
                res.render('register', error);
                return;
            }

            passport.authenticate('local')(req, res, function () {
                res.redirect('/');
            });
        });
    }
});

router.get('/login', function(req, res) {
        res.render('login', { user : req.user });
});

router.post('/login',
    passport.authenticate('local', { failWithError: true }),
    function(req, res, next) {
        // handle success
        // user redirected to index page, authenticated
        return res.redirect('/');
    },
    function(err, req, res, next) {
        // handle error
        // render login page again and attach our error
        res.render('login', {error: {message: err.message}});
    }
);

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/');
});

module.exports = router;
