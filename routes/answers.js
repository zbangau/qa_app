var express = require('express');
var mongoose = require('mongoose');
var ObjectId = mongoose.Types.ObjectId;
var router = express.Router();

var Answer = require('../models/answer');

router.put('/answer/:id', function (req, res) {
    query = Answer.findOne({"_id" : req.params.id});
    query.select('body user');
    query.exec(function(err, obj){
        if (err){
            console.log(err);
        } else {
            console.log( req.body );
            if (String(req.user._id) != String(obj.user)) {
                res.status(403).send("Unauthorized");
                return;
            }
            if (!req.body.body){
                res.status(400).send("Body is wrong format");
                return;
            }
            obj.body = req.body.body;
            obj.lastModified = Date.now();
            obj.save(function(err){if (err) console.log(err)});
            res.setHeader('Content-Type', 'application/json');
            res.send(JSON.stringify({"body": obj.body, "edited": obj.lastModified}));
        }
    });
});

module.exports = router;